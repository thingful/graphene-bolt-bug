# graphene-bolt-bug

This code is to recreate the error: `Couldn't read expected bytes for message length. Read: 1 Expected: 2.` that we are currently having.
The code mimics the behavior of the bug which happens when we try to read something from Neo4j.

The code will spawns a bunch of Go routines, each will try to make a bunch of simle queries to Neo4j.

### requirement
`go get github.com/johnnadratowski/golang-neo4j-bolt-driver`

### usage
`go run clients.go`
`usage: username password maxConn processNum queryNum`
`maxConn` : Maximm number of connection
`processNum`: Number of Go routines
`queryNum` : Number of queries each Go routine will make

I could recreate the error quite reliably at `go run clients.go USERNAME PASSWORD 2 2 200`
