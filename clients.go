package main

import (
	"fmt"
	"os"
	"strconv"
	"sync"

	bolt "github.com/johnnadratowski/golang-neo4j-bolt-driver"
)

const (
	boltAddress = "bolt://%s:%s@db-bw6tvnzpycpfrzvj30mu.graphenedb.com:24786?tls=true&tls_no_verify=false"
)

func main() {

	var username string
	var pw string
	var processNum int
	var queryNum int
	var maxConn int

	args := os.Args
	if len(args) != 6 {
		fmt.Println("usage: username password maxConn processNum queryNum")
		return
	} else {
		username = args[1]
		pw = args[2]
		maxConn, _ = strconv.Atoi(args[3])
		processNum, _ = strconv.Atoi(args[4])
		queryNum, _ = strconv.Atoi(args[5])
	}

	var wg sync.WaitGroup

	driverPool, err := bolt.NewDriverPool(fmt.Sprintf(boltAddress, username, pw), maxConn)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("starting spamSearch processNum:%d, queryNum:%d\n", processNum, queryNum)

	for i := 0; i < processNum; i++ {
		wg.Add(1)
		go func(driverPool bolt.DriverPool, i int, queryNum int) {
			defer wg.Done()
			testSearchLoop(driverPool, i, queryNum)

		}(driverPool, i, queryNum)

	}

	wg.Wait()

}

func testSearchLoop(driverPool bolt.DriverPool, round int, queryNum int) {

	fmt.Printf("process no%d, queryNum:%d\n", round, queryNum)

	conn, err := driverPool.OpenPool()
	if err != nil {
		fmt.Println(err)
		return
	}

	defer conn.Close()

	// this is just a pool of urls to search for
	urls := []string{
		"http://purl.org/iot/vocab/thingful#ConnectedDevice",
		"http://purl.org/iot/vocab/m3-lite#AirTemperature",
		"http://purl.org/iot/vocab/m3-lite#DegreeCelsius",
		"http://purl.org/iot/vocab/m3-lite#Weather",
		"http://purl.org/iot/vocab/m3-lite#AirThermometer",
		"http://purl.org/iot/vocab/m3-lite#AtmosphericPressure",
		"http://purl.org/iot/vocab/thingful/qu#Hectopascal",
		"http://purl.org/iot/vocab/m3-lite#AtmosphericPressureSensor",
		"http://purl.org/iot/vocab/thingful/qu#MeanSeaLevelPressure",
		"http://purl.org/iot/vocab/m3-lite#Humidity",
		"http://purl.org/iot/vocab/m3-lite#Percent",
		"http://purl.org/iot/vocab/m3-lite#HumiditySensor",
		"http://purl.org/iot/vocab/m3-lite#Rainfall",
		"http://purl.org/iot/vocab/m3-lite#Millimetre",
		"http://purl.org/iot/vocab/m3-lite#WindSpeed",
		"http://purl.org/iot/vocab/m3-lite#MetrePerSecond",
		"http://purl.org/iot/vocab/m3-lite#WindSpeedSensor",
		"http://purl.org/iot/vocab/m3-lite#WindDirection",
		"http://purl.org/iot/vocab/m3-lite#DegreeAngle",
		"http://purl.org/iot/vocab/m3-lite#WindDirectionSensor",
		"http://purl.org/iot/vocab/m3-lite#CloudCover",
		"http://purl.org/iot/vocab/m3-lite#CloudCoverSensor",
		"http://purl.org/iot/vocab/thingful/qu#SunriseTime",
		"http://purl.org/iot/vocab/thingful/qu#SunsetTime",
	}

	for i := 0; i < queryNum; i++ {
		// the actual query is just checking if urls existed in the db
		ontologyQuery := fmt.Sprintf(`MATCH (o:OntologyClass {id: "%s" }) RETURN o.id`, urls[i%len(urls)])
		data, _, _, err := conn.QueryNeoAll(ontologyQuery, nil)

		if err != nil {
			fmt.Printf("process:%d, queryNum:%d err:%v\n", round, queryNum, err)
			return
		}
		if len(data) != 1 {
			fmt.Printf("process:%d, queryNum:%d can't find it\n", round, queryNum)
		}
	}

}
